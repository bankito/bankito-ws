const config = require('./config');
const express = require('express');
const mongoose = require('mongoose');
var cors = require('cors');

const app = express();
app.use(cors());

app.use(require('./routes/movimiento'))
app.use(require('./routes/usuario'))
app.use(require('./routes/session'))
app.use(require('./routes/moneda'))
app.use(require('./routes/persona'))
app.use(require('./routes/cuenta'))

// Conectando a BD
// mongoose.connect('mongodb://mongo:27017/bankito')
//     .then(db => console.log('BD Conectada'))
//     .catch(err => console.log('Error de conexión a la BD'));
mongoose.connect('mongodb+srv://root:p4ssword@cluster0-14c3f.gcp.mongodb.net/bankito?retryWrites=true')
    .then(db => console.log('BD Conectada a mongodb.net'))
    .catch(err => console.log('Error de conexión a la BD'));

app.listen(config.app.port, () => {
    console.log('Server on port '+config.app.port);
})
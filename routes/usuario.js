const express = require('express');
const bcrypt = require('bcrypt');
const bodyParser = require('body-parser');
const Usuario = require('../models/usuario');
const UsuarioDetalle = require('../models/usuarioDetalle');
const UsuarioCuenta = require('../models/usuarioCuenta');
const VerifyToken = require('../auth/VerifyToken');

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

app.get('/usuario', VerifyToken, (req, res, next) => {

    Usuario.find({ }).exec((err, usuarios) => {
        if(err){
            return res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok: true,
            usuarios
        })
    })
})

app.post('/usuario', (req, res) => {
    let body = req.body;
    let usuario = new Usuario({
        user: body.username,
        email: body.mail,
        password: bcrypt.hashSync(body.password, 10),
        state: true
    });

    Usuario.create( usuario, (err) => {
        if(err){
            console.log("Error:" + err)
            res.status(200).send({
                ok: false,
                msg: err
            })
        }else{
            let usuarioDetalle = new UsuarioDetalle({
                user: body.username,
                first_name: body.nombre,
                last_name: body.apellido,
                id_documento_identidad: body.tipoDocumento,
                numero_documento: body.dni == "" ? body.ruc : body.dni,
                razon_social: body.razonSocial
            })
            UsuarioDetalle.create( usuarioDetalle, err1 => {
                if(err1){
                    console.log("Error:" + err1)
                    Usuario.deleteOne({user: usuario.user}, (err3) => {
                        console.log("Se realiza rollback del usuario " + usuario.user)
                    });
                    res.status(200).send({
                        ok: false,
                        msg: err1
                    })
                }else{
                    res.status(200).send({
                        ok: true,
                        msg: 'Usuario creado'
                    });
                }
            });
        }
    });
    
})

app.get('/usuario/:user', VerifyToken, (req, res) => {
    let user = req.params.user;
    let body = req.body;

    Usuario.findOne({ user : user})
        .exec((err, usuario) => {
            if(err){
                return res.status(400).json({
                    ok: false,
                    err
                })
            }
            UsuarioDetalle.findOne({ user : user})
                .exec((err, usuario_detalle) => {
                if(err){
                    return res.status(400).json({
                        ok: false,
                        err
                    })
                }
                console.log(usuario_detalle)
                res.status(200).json({
                    ok: true,
                    usuario: {
                        user: usuario.user,
                        email: usuario.email,
                        id_documento_identidad: usuario_detalle.id_documento_identidad,
                        numero_documento: usuario_detalle.numero_documento,
                        first_name: usuario_detalle.first_name,
                        last_name: usuario_detalle.last_name,
                        razon_social: usuario_detalle.razon_social
                    }
                })
            });
        })
})

app.post('/usuario/:user', VerifyToken, (req, res) => {
    let user = req.params.user;
    let body = req.body;

    Usuario.findByIdAndUpdate(user, body, {new: true}, (err, usuarioDB) =>{
        if (err) {
            return res.status(400).json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            usuario: usuarioDB
        });
    })
})

app.delete('/usuario', VerifyToken, (req, res) => {
    res.json('get usuario')
})

module.exports = app;
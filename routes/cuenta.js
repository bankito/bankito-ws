const express = require('express');
const bcrypt = require('bcrypt');
const bodyParser = require('body-parser');
const UsuarioCuenta = require('../models/usuarioCuenta');
const VerifyToken = require('../auth/VerifyToken');

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

app.post('/cuenta', VerifyToken, (req, res) => {
    
    let body = req.body;
    
    let cuenta = new UsuarioCuenta({
        user: body.user,
        alias: body.alias,
        moneda: body.tipoCuenta,
        cuenta: body.cuenta,
        id_documento_identidad: body.tipoDocumento,
        numero_documento: body.dni == "" ? body.ruc : body.dni
    })
    UsuarioCuenta.create( cuenta, err => {
        if(err){
            console.log("Error:" + err)
            res.status(200).send({
                ok: false,
                msg: err
            })
        }else{
            res.status(200).send({
                ok: true,
                msg: 'Cuenta creada'
            });
        }
    });
})

app.get('/cuenta/:user', VerifyToken, (req, res) => {
    let user = req.params.user;
    let body = req.body;

    UsuarioCuenta.find({ user : user})
        .exec((err, cuentas) => {
            if(err){
                return res.status(400).json({
                    ok: false,
                    err
                })
            }
            res.json({
                ok: true,
                cuentas
            })
        })
})

module.exports = app;
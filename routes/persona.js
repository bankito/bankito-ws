const config = require('../config');
const express = require('express');
const Request = require("request");
const bodyParser = require('body-parser');

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

app.get('/persona/dni/:dni', (req, res, next) => {
    
    let url=config.api_dni.endpoint+req.params.dni

    Request.get(url, (error, response, body) => {
        if(error) {
            return console.dir(error);
        }
        let arr = body.split("|");

        res.status(200).send({
            ok: true,
            apellidos: arr[0]+" "+arr[1],
            nombres: arr[2]
        })
    });
})

app.get('/persona/ruc/:ruc', (req, res, next) => {

    let url=config.api_ruc.endpoint+req.params.ruc

    Request.get(url, (error, response, body) => {
        if(error) {
            return console.dir(error);
        }
        let empresa = JSON.parse(body);
        res.status(200).send({
            ok: true,
            razon_social: empresa.razon_social,
            ruc: empresa.ruc
        })
        
    });
})

module.exports = app;
const config = require('../config');
const express = require('express');
const Usuario = require('../models/usuario');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const app = express();

var secretText = config.jwt.key;

app.post('/login', (req, res) => {
    console.log(req.body.username)

    Usuario.findOne({ 'user' : req.body.username }, function (err, user) {
        if (user != null) {
            bcrypt.compare(req.body.password, user.password, function(err, rspt) {
                if(rspt == true){
                    console.log("Usuario autenticado");
                    var token = jwt.sign({username: user._id}, secretText, {
                        expiresIn: 60 * 60 * 1 // expires in 1 hour
                    })

                    res.status(200).send({
                        "auth": true,
                        "user": user.user,
                        "email": user.email,
                        "token": token
                    })
                }else{
                    console.log("Credenciales invalidas");
                    res.status(200).send({
                        "auth": false,
                        "msg": "Credenciales invalidas"
                    })
                }
            });
        }else{
            console.log("Usuario no existe");
            res.status(200).send({
                "auth": false,
                "msg": "Usuario no existe"
              })
        }
        
    });

})

app.get('/logout', (req, res) => {
    res.status(200).send({ auth: false, token: null });
})

module.exports = app;
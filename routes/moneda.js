const config = require('../config');
const express = require('express');
const Request = require("request");

const app = express();

app.get('/moneda/convert_usd', (req, res, next) => {

    var url=config.api_currency.endpoint+"convert?apiKey="+config.api_currency.api_key+"&q=USD_PEN&compact=ultra&"

    Request.get(url, (error, response, body) => {
        if(error) {
            return console.dir(error);
        }
        let currency = JSON.parse(body);
        let tasa = 0.008
        console.log(currency.USD_PEN)
        res.status(200).send({
            ok: true,
            exchange: currency.USD_PEN.toFixed(3),
            compra: (currency.USD_PEN*(1+tasa)).toFixed(3),
            venta: (currency.USD_PEN*(1-tasa)).toFixed(3)
        })
        
    });
})

app.get('/moneda/convert_btc', (req, res, next) => {

    var url="https://blockchain.info/ticker"

    Request.get(url, (error, response, body) => {
        if(error) {
            return console.dir(error);
        }
        let currency = JSON.parse(body);
        let tasa = 0.1
        console.log(currency.USD.last)
        res.status(200).send({
            ok: true,
            exchange: currency.USD.last.toFixed(3),
            compra: (currency.USD.last*(1+tasa)).toFixed(3),
            venta: (currency.USD.last*(1-tasa)).toFixed(3)
        })
        
    });
})

module.exports = app;
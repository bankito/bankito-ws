const express = require('express');
const Movimiento = require('../models/movimiento');
const bodyParser = require('body-parser');
const VerifyToken = require('../auth/VerifyToken');


const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

app.get('/movimiento', VerifyToken, (req, res) => {
    Movimiento.find({ }).exec((err, movimientos) => {
        if(err){
            return res.status(400).json({
                ok: false,
                err
            })
        }
        res.json({
            ok: true,
            movimientos
        })
    })
})

app.get('/movimiento/:user', VerifyToken, (req, res) => {
    let user = req.params.user;

    Movimiento.find({ user : user})
        .exec((err, movimientos) => {
            if(err){
                return res.status(400).json({
                    ok: false,
                    err
                })
            }
            res.json({
                ok: true,
                movimientos
            })
        })
})

app.post('/movimiento', VerifyToken, (req, res) => {
    
    let body = req.body;
    console.log(req.body)
    let movimiento = new Movimiento({
        user: body.user,
        tipo_operacion: body.tipoOperacion,
        cuenta_alias: body.cuenta_sel,
        num_operacion: body.numOperacion,
        tasa_cambio: body.tipoOperacion == "compra" ? body.dolar_compra : body.dolar_venta,
        venta_deposite: body.tipoOperacion == "venta" ? body.dolar_venta_tengo : null,
        venta_recibi: body.tipoOperacion == "venta" ? body.dolar_venta_recibo : null,
        compra_deposite: body.tipoOperacion == "compra" ? body.dolar_compra_tengo : null,
        compra_recibi: body.tipoOperacion == "compra" ? body.dolar_compra_recibo : null
    })
    Movimiento.create( movimiento, err => {
        if(err){
            console.log("Error:" + err)
            res.status(200).send({
                ok: false,
                msg: err
            })
        }else{
            res.status(200).send({
                ok: true,
                msg: 'Operación creada'
            });
        }
    });
})

app.put('/movimiento/:id', VerifyToken, (req, res) => {
    let id = req.params.id;
    if(id === undefined){
        res.status(400).json({
            ok: false,
            mensaje: 'Id no existe'
        })
    }else{
        res.json({
            id
        })
    }
    
})

app.delete('/movimiento', VerifyToken, (req, res) => {
    res.json('get usuario')
})

module.exports = app;
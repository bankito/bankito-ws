const config = {
    app: {
      port: 3000
    },
    db: {
      host: 'mongo',
      port: 27017,
      name: 'bankito'
    },
    jwt: {
      key: 'P4$$W0RD'
    },
    api_currency: {
      endpoint: 'https://free.currencyconverterapi.com/api/v6/',
      api_key: 'f960d1b556b05b927969'
    },
    api_ruc:{
      endpoint: 'https://api.sunat.cloud/ruc/'
    },
    api_dni:{
      endpoint: 'http://aplicaciones007.jne.gob.pe/srop_publico/Consulta/Afiliado/GetNombresCiudadano?DNI='
    }
   };
   
   module.exports = config;
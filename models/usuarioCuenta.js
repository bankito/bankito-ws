const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let userCuentaSchema = new Schema({
    user: {
        type: String,
        required: [true, 'El usuario es necesario']
    },
    alias: {
        type: String,
        required: [true, 'El alias es necesario']
    },
    moneda: {
        type: String,
        required: [true, 'La moneda es necesario']
    },
    cuenta: {
        type: String,
        required: [true, 'El número de cuenta es necesario']
    },
    id_documento_identidad: {
        type: String,
        required: [true, 'El codigo de documento es necesario']
    },
    numero_documento: {
        type: String,
        required: [true, 'El numero de documento es necesario']
    }
}, { collection: 'user_cuenta' });
userCuentaSchema.index({ user: 1, alias: 1 }, {unique: true});

module.exports = mongoose.model('user_cuenta', userCuentaSchema);
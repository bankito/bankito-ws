const mongoose = require('mongoose');
// const uniqueValidator = require('mongoose-unique-validator');

let Schema = mongoose.Schema;

let usuarioSchema = new Schema({
    user: {
        type: String,
        required: [true, 'El usuario es necesario'],
        unique: true
    },
    email: {
        type: String,
        required: [true, 'El mail es necesario'],
        unique: true
    },
    password: {
        type: String,
        required: [true, 'La clave es necesaria']
    },
    state: {
        type: Boolean,
        required: [true, 'El id es necesario'],
        default: true
    }
}, { collection: 'user' });

module.exports = mongoose.model('user', usuarioSchema);
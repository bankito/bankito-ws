const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let movimientoSchema = new Schema({
    user: {
        type: String
    },
    fecha: {
        type: Date,
        default: Date.now
    },
    tipo_operacion: {
        type: String
    },
    cuenta_alias: {
        type: String
    },
    num_operacion: {
        type: String
    },
    tasa_cambio: {
        type: Number
    },
    venta_deposite: {
        type: Number
    },
    venta_recibi: {
        type: Number
    },
    compra_deposite: {
        type: Number
    },
    compra_recibi: {
        type: Number
    }
}, { collection: 'movimiento' });

module.exports = mongoose.model('movimiento', movimientoSchema);
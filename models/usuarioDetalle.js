const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let userDetailSchema = new Schema({
    user: {
        type: String,
        required: [true, 'El usuario es necesario'],
        unique: true
    },
    id_documento_identidad: {
        type: String,
        required: [true, 'El codigo de documento es necesario']
    },
    numero_documento: {
        type: String,
        required: [true, 'El numero de documento es necesario']
    },
    first_name: {
        type: String
    },
    last_name: {
        type: String
    },
    razon_social:{
        type: String
    }
}, { collection: 'user_detail' });

module.exports = mongoose.model('user_detail', userDetailSchema);